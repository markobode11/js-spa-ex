<?php

$url  = parse_url($_SERVER['REQUEST_URI']);
$file = __DIR__ . $url['path'];
if (is_file($file)) {
    // Kui skript tagastab 'false', siis tagastatakse kasutajale fail, mida küsiti
    return false;
} else {
    // Kasutajale väljastatakse antud skripti väljund
    echo "Hello from router.php";
}
