(async () => {

    // Kirjutage kood siia

})();

async function fetchCatFacts() {
    return await fetch("https://cat-fact.herokuapp.com/facts/random")
        .then(response => response.json());
}